require('dotenv').config()
// console.log(process.env.POSTGRESS_USER, "test");
const express = require('express')
const errorHandler = require('./middlewares/errorhandler')
const app = express()
const port = 3000
const router = require("./routes")

app.use(express.json())
app.use(express.urlencoded({extended: false}));

app.use(router)
app.use(errorHandler)

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})