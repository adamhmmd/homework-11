const express = require("express");
const router = express.Router();
const ItemController = require("../controllers/itemController.js")

router.get("/todo", ItemController.findAll);
router.get("/todo/:id", ItemController.findOne);
router.post("/todo", ItemController.create);

module.exports = router;