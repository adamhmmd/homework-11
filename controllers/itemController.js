const {Item} = require("../models")

class ItemController {

  static findAll = async(req, res, next) => {
    
    try {
      const data = await Item.findAll();
    
      res.status(200).json(data)

    } catch(err){
      next(err)
    }
    
  }

  static findOne = async(req, res, next) => {
    const {id} = req.params;

    try {
      const data = await Item.findOne({
        where: {
          id
        }
      })
      if(data) {
        res.status(200).json(data)
      } else {
        throw {name : "ErrorNotFound"}
      }

    } catch(err){
      next(err)
    }
  }

  static create =  async(req, res, next) => {

    try {
      const {agenda, location, status} = req.body;
      const data = await Item.create({
        agenda,
        location,
        status
      });
    
      res.status(200).json(data)

    } catch(err){
      next(err)
    }
  }

  static destroy = async(req, res, next) => {

    try {
      const {id} = req.params;
      const data = await Item.update({
        status: "Done"
      }, {
          where: (
            id
          )

      })

      if(data [0] === 1) {
        res.status(200).json({
          message: "Delete Done"
        })
      } else {
        throw {name: "ErrorNotFound"}
      }

    } catch(err) {
      next(err)
    }
  }
}


module.exports = ItemController;