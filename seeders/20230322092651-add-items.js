'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('Items', [
      {
        agenda: "Cuci Piring",
        location: "Rumah",
        status: "Done",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        agenda: "Kondangan",
        location: "Hotel ABC",
        status: "Not Done Yet",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        agenda: "Garap HW",
        location: "Rumah",
        status: "Not Done Yet",
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ])
    
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('items', null, {});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
